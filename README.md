# Barkode Logger #

## Table of contents ##
1. [Purpose](#purpose)
2. [Technologies](#technologies)
3. [Getting started](#getting started)

    1. [Setup instructions](#setup instructions)

## Purpose
The purpose of this small program is to populate a database with information about objects and viewing them in a desktop program.
The objects are identified by a unique code (as in a barcode) and can be given a nickname.
 
When something happens to an object, i.e. the frequency of a radio was adjusted, this can be added to a log of events for that object.

## Technologies ##
As part of my learning process as a computer scientist, I always strive to challenge myself by discovering and learning new programming languages and concepts.

One such language is an up and coming one.
[Kotlin](https://kotlinlang.org/ "Kotlin Website"), a language developed by the good people of [JetBrains](https://www.jetbrains.com/), is a language that I already had a chance to learn for a project involving Android Development. 

As a next step in discovering the possibilities of the language, 
I wanted to try and create a desktop application combining Kotlin and JavaFX. A framework that combines these two languages is [TornadoFX](https://github.com/edvin/tornadofx "TornadoFX's GitHub page").

## Getting started ##
### Setup instructions ###
In order to run the jar file, a connection to a MariaDB database `barcode_logger` has to be established with the proper requirements.

First, create the database as the root MySQL user using a terminal (assuming you've installed a MariaDB server correctly).

```
$ mysql -u root -p
MariaDB [(none)]> CREATE DATABASE barkode_logger;
```

Next, create a user called `barkode` with password `barkode` and grant him the newly created database.

```
MariaDB [(none)]> CREATE USER 'barkode'@'localhost' IDENTIFIED BY 'barkode';
MariaDB [(none)]> GRANT ALL PRIVILEGES ON barkode_logger.* TO 'barkode'@'localhost';
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> quit
```
