package models

import org.joda.time.DateTime

data class Event(val date: DateTime, val text: String) {
    override fun toString(): String {
        return text+";"+date.toLocalDate().toString()
    }
}