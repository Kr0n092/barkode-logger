package models

import javafx.collections.ObservableList
import org.joda.time.DateTime
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 1/07/17
 */
class ScanObject(code: String = "", name: String = "", date: DateTime = DateTime.now(), amount: Int = 1) {
    var code by property<String>()
    fun codeProperty() = getProperty(ScanObject::code)

    var name by property<String>()
    fun nameProperty() = getProperty(ScanObject::name)

    var amount: Int

    val created: DateTime by lazy { date }

    val events: ObservableList<Event>

    init {
        this.code = code
        this.name = name
        this.amount = amount
        this.events = arrayListOf<Event>().observable()
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append(name, ",")
        sb.append(code, ",")
        sb.append(amount, ",")
        sb.append(created.toLocalDate().toString(), ",")
        sb.append(printEvents())

        return sb.toString()
    }

    fun addEvent(text: String) {
        if (text.isNullOrBlank()) {
            return
        } else {
            this.events.add(Event(DateTime.now(), text))
        }
    }

    fun addEvent(date: DateTime, text: String) {
        if (text.isNullOrBlank()) {
            return
        } else {
            this.events.add(Event(date, text))
        }
    }

    fun printEvents(): String {
        val sb: StringBuilder = StringBuilder()
        for (i in 0..events.size - 1) {
            sb.append(events[i].toString())
            if (i < events.size - 1) {
                sb.append(",")
            }
        }

        return sb.toString()
    }
}