package models

/**
 * @author Olivier Brabanders
 * Created on 3/07/17
 */
object ScanObjectFactory {
    fun objectList(max: Int = 10): ArrayList<ScanObject> {
        val objectList: ArrayList<ScanObject> = arrayListOf()
        for (i in 0..max) {
            val scanObject: ScanObject = ScanObject()
            scanObject.code = i.toString()
            scanObject.name = "Object $i"
            scanObject.amount = i
            objectList.add(scanObject)
        }
        return objectList
    }
}