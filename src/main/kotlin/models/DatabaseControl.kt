package models

import javafx.collections.ObservableList
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 4/07/17
 */
object Objects : Table("Objects") {
    val id = integer("id").primaryKey().autoIncrement()
    val code = text("code")
    val name = text("name")
    val amount = integer("amount")
    val created = date("created")
    val events = text("events")

}

object DatabaseControl {

    fun setUp() {
        Database.connect("jdbc:mysql://localhost:3306/barkode_logger", driver = "com.mysql.jdbc.Driver", user = "barkode", password = "barkode")
        transaction {
            create(Objects)
        }
    }

    fun wipe() {
        transaction {
            Objects.deleteAll()
        }
    }

    fun add(objects: ArrayList<ScanObject>) {
        objects.forEach {
            add(it)
        }
    }

    fun delete(objectToDelete: ScanObject) {
        transaction {
            Objects.deleteWhere {
                (Objects.code eq objectToDelete.code) and (Objects.name eq objectToDelete.name) and (Objects.created eq objectToDelete.created)
            }
        }
    }

    fun select(keyText: String): ObservableList<ScanObject> {
        val result: ArrayList<ScanObject> = arrayListOf()
        transaction {
            Objects.select({ (Objects.code eq keyText) or (Objects.name like "$keyText%") }).forEach {
                result.add(createObject(it))
            }
        }
        return result.observable()
    }

    fun add(scannedObject: ScanObject) {
        transaction {
            Objects.insert {
                it[code] = scannedObject.code
                it[name] = scannedObject.name
                it[amount] = scannedObject.amount
                it[created] = scannedObject.created
                it[events] = ""
            }
        }
        if (scannedObject.events.isNotEmpty()) {
            addEvent(scannedObject)
        }
    }

    fun updateObject(oldObject: ScanObject, newCode: String, newName: String, newAmount: Int) {
        transaction {
            Objects.update({ (Objects.code eq oldObject.code) and (Objects.name eq oldObject.name) }) {
                it[code] = newCode
                it[name] = newName
                it[amount] = newAmount
            }
        }
    }

    fun addEvent(scannedObject: ScanObject) {
        transaction {
            Objects.update({ (Objects.code eq scannedObject.code) and (Objects.name eq scannedObject.name) }) {
                it[events] = scannedObject.printEvents()
            }
        }
    }

    fun objects(): ObservableList<ScanObject> {
        val objectList: ArrayList<ScanObject> = arrayListOf()

        transaction {
            Objects.selectAll().forEach {
                objectList.add(createObject(it))
            }
        }

        return objectList.observable()
    }

    private fun createObject(row: ResultRow): ScanObject {
        val newObject: ScanObject = ScanObject(row[Objects.code], row[Objects.name], row[Objects.created], row[Objects.amount])
        row[Objects.events].split(",").forEach {
            if (!it.isNullOrBlank()) {
                val event: List<String> = it.split(";")
                newObject.addEvent(DateTime.parse(event[1]), event[0])
            }
        }
        return newObject
    }
}
