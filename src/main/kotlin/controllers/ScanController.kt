package controllers

import eventbus.ObjectRequest
import eventbus.ObjectSendEvent
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 1/07/17
 */
object ScanController: Controller() {
    private lateinit var scanObject: ScanObject
    init {
        subscribe<ObjectRequest> {
            fire(ObjectSendEvent(scanObject))
        }
    }

    fun setObject(scanObject: ScanObject) {
        this.scanObject = scanObject
    }
}