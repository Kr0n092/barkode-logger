package controllers

import eventbus.NewObjectEvent
import models.DatabaseControl
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 5/07/17
 */
class ObjectController(private val responsibleObject: ScanObject) : Controller() {
    fun update(code: String, name: String, amount: Int) {
        runAsync { DatabaseControl.updateObject(responsibleObject, code, if (!name.isNullOrBlank()) name else code, amount) }
    }

    fun addEventToObject(event: String) {
        responsibleObject.addEvent(event)
        runAsync { DatabaseControl.addEvent(responsibleObject) }
    }

    fun delete() {
        runAsync { DatabaseControl.delete(responsibleObject) }
    }

    fun addObject(amount: Int) {
        runAsync {
            if (responsibleObject.name.isNullOrBlank()) {
                responsibleObject.name = responsibleObject.code
            }
            responsibleObject.amount = amount
            DatabaseControl.add(responsibleObject)
            fire(NewObjectEvent(DatabaseControl.objects()))
        }

    }
}