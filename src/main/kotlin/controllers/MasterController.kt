package controllers

import eventbus.ObjectSearchEvent
import javafx.collections.ObservableList
import models.DatabaseControl
import models.ScanObject
import tornadofx.*

class MasterController : Controller() {
    fun objects(): ObservableList<ScanObject> {
        return DatabaseControl.objects()
    }

    fun search(searchText: String) {
        runAsync {
            fire(ObjectSearchEvent(DatabaseControl.select(searchText)))
        }
    }
}