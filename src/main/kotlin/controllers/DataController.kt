package controllers

import models.CSVWriter
import models.DatabaseControl
import models.ScanObject
import org.joda.time.DateTime
import tornadofx.*
import java.io.File
import java.io.FileReader
import java.io.FileWriter

/**
 * @author Olivier Brabanders
 * Created on 6/07/17
 */
class DataController : Controller() {
    private fun toCSV(vararg headers: String): String {
        val csvWriter = CSVWriter(*headers)

        DatabaseControl.objects().forEach {
            csvWriter.addRow(it.toString())
        }

        return csvWriter.toString()
    }

    private fun fromCSV(csvContent: List<String>): ArrayList<ScanObject> {
        val content: ArrayList<ScanObject> = arrayListOf()
        csvContent.forEach {
            val splitData = it.split(",")
            val basicData: List<String> = splitData.subList(0, 3)
            val scanObject = ScanObject(basicData[1], basicData[0], DateTime(basicData[2]))
            val eventData: List<String> = splitData.subList(3, splitData.size)
            eventData.forEach {
                if (!it.isNullOrBlank()) {
                    val splitEvent = it.split(";")
                    scanObject.addEvent(DateTime(splitEvent[1]), splitEvent[0])
                }
            }
            content += scanObject
        }

        return content
    }

    fun save(location: String, vararg headers: String) {
        runAsync {
            val objectsAsCSV: String = toCSV(*headers)
            val writer = FileWriter(location + File.separator + "barkode.csv")
            writer.write(objectsAsCSV)
            writer.flush()
            writer.close()
        }
    }

    fun load(location: List<File>) {
        location.forEach {
            runAsync {
                val reader = FileReader(it)
                val content: List<String> = reader.readLines()
                val contentWithoutHeader = content.drop(1)
                val objects: ArrayList<ScanObject> = fromCSV(contentWithoutHeader)
                DatabaseControl.wipe()
                DatabaseControl.add(objects)
            }
        }
    }
}