import javafx.stage.Stage
import models.DatabaseControl
import tornadofx.*
import views.MasterView

/**
 * @author Olivier Brabanders
 * Created on 2017-07-01
 */
class MyApp : App(MasterView::class) {
    init {
        DatabaseControl.setUp()
    }
    override fun start(stage: Stage) {
        super.start(stage)
        stage.minWidth = 700.0
        stage.minHeight = 700.0
    }
}