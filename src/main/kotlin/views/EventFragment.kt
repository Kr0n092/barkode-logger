package views

import controllers.DataController
import controllers.ObjectController
import eventbus.ObjectRequest
import eventbus.ObjectSendEvent
import javafx.scene.control.Button
import javafx.scene.control.TextField
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 3/07/17
 */
class EventFragment : Fragment() {
    override val root = Form()
    private val controller: ObjectController by lazy { ObjectController(scannedObject) }
    private lateinit var scannedObject: ScanObject
    private val eventText: TextField = TextField()
    private val okButton: Button = Button(messages["saveEvent"])

    init {
        fire(ObjectRequest)
        subscribe<ObjectSendEvent> {
            scannedObject = it.requestedObject
            with(okButton) {
                action {
                    controller.addEventToObject(eventText.text)
                    close()
                }
            }
        }
        with(eventText) {
            setOnKeyReleased {
                okButton.isDisable = text.isNullOrBlank()
            }
        }


        with(root) {
            fieldset {
                field(messages["enterEvent"]) {
                    this += eventText
                }
            }

            this += okButton
        }
    }
}
