package views

import controllers.ObjectController
import controllers.ScanController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import eventbus.ObjectRequest
import eventbus.ObjectSendEvent
import javafx.scene.control.Button
import javafx.scene.control.Spinner
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import models.Event
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 3/07/17
 */
class ObjectDetailFragment : Fragment("Details") {
    private lateinit var scannedObject: ScanObject
    private val controller: ObjectController by lazy { ObjectController(scannedObject) }
    override val root = BorderPane()
    private val nameField: TextField by lazy { createDumbTextField(scannedObject.name) }
    private val codeField: TextField by lazy { createDumbTextField(scannedObject.code) }
    private val dateField: TextField by lazy { createDumbTextField(scannedObject.created.toLocalDate().toString()) }
    private val amountField: Spinner<Int> by lazy { createDumbSpinner(scannedObject.amount) }
    private val editButton: Button = Button(messages["save"])

    init {
        fire(ObjectRequest)
        subscribe<ObjectSendEvent> {
            event ->
            scannedObject = event.requestedObject
            root.center {
                form {
                    fieldset(messages["detail"]) {
                        field(messages["name"]) {
                            this += nameField
                        }
                        field(messages["code"]) {
                            this += codeField
                        }
                        field(messages["amount"]) {
                            this += amountField
                        }
                        field(messages["date"]) {
                            this += dateField
                        }
                        field(messages["events"]) {
                            tableview(scannedObject.events) {
                                column(messages["date"], Event::date).cellFormat {
                                    text = it.toLocalDate().toString()
                                }
                                column(messages["text"], Event::text)
                                columnResizePolicy = SmartResize.POLICY
                            }
                        }
                    }
                }
            }
        }
        editButton.isDisable = true
        with(root) {
            prefHeight = 600.0
            prefWidth = 700.0
            top {
                menubar {
                    menu(messages["menu"]) {
                        item(messages["modify"], graphic = FontAwesomeIconView(FontAwesomeIcon.EDIT)) {
                            action {
                                nameField.isEditable = true
                                codeField.isEditable = true
                                amountField.isEditable = true
                                editButton.isDisable = false
                                editButton.setOnAction {
                                    nameField.isEditable = false
                                    codeField.isEditable = false
                                    amountField.isEditable = false
                                    editButton.isDisable = true
                                    controller.update(codeField.text, nameField.text, amountField.value)
                                }
                            }
                        }
                        item(messages["delete"], graphic = FontAwesomeIconView(FontAwesomeIcon.REMOVE)) {
                            action {
                                controller.delete()
                                close()
                            }
                        }
                    }
                }
            }

            right {
                vbox {
                    button(messages["addEvent"]) {
                        action {
                            openEventWindow()
                        }
                    }
                    this += editButton
                }

            }
        }
    }

    private fun openEventWindow() {
        ScanController.setObject(scannedObject)
        this.find<EventFragment>().openModal()
    }

    private fun createDumbTextField(text: String): TextField {
        val textField: TextField = TextField(text)
        textField.isEditable = false
        return textField
    }

    private fun createDumbSpinner(amount: Int): Spinner<Int> {
        val spinner: Spinner<Int> = IntegerSpinner(amount)
        spinner.isEditable = false
        return spinner
    }
}
