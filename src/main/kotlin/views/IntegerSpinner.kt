package views

import javafx.scene.control.Spinner

/**
 * @author Olivier Brabanders
 * Created on 8/08/17
 */
class IntegerSpinner(initialAmount: Int): Spinner<Int>(0, 99, initialAmount)