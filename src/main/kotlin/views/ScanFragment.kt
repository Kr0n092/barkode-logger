package views

import controllers.ObjectController
import javafx.scene.control.Spinner
import javafx.scene.control.TextField
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 1/07/17
 */
class ScanFragment : Fragment() {
    override val root = Form()
    private val codeField: TextField = TextField()
    private val nameField: TextField = TextField()
    private val amountField: Spinner<Int> = IntegerSpinner(1)
    val controller: ObjectController by lazy { ObjectController(scanObject) }
    val scanObject: ScanObject by lazy { ScanObject() }

    init {
        with(codeField) {
            bind(scanObject.codeProperty())
        }
        with(nameField) {
            bind(scanObject.nameProperty())
        }
        with(root) {
            fieldset {
                field(messages["scan"]) {
                    this += codeField
                }
                field(messages["name"]) {
                    this += nameField
                }
                field(messages["amount"]) {
                    this += amountField
                }
            }
            button(messages["save"]) {
                disableProperty().bind(scanObject.codeProperty().isNull)
                action {
                    controller.addObject(amountField.value)
                    close()
                }
            }
        }
    }
}
