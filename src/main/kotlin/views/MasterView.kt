package views

import controllers.DataController
import controllers.MasterController
import controllers.ScanController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import eventbus.NewObjectEvent
import eventbus.ObjectSearchEvent
import javafx.scene.control.Button
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.stage.FileChooser
import models.ScanObject
import org.controlsfx.control.Notifications
import tornadofx.*


class MasterView : View() {
    private val controller: MasterController by inject()
    private val dataController: DataController by inject()
    private val searchField: TextField = TextField(messages["search_object"])
    private val objectTable: TableView<ScanObject> = TableView()
    private val detailButton: Button = Button(messages["view_details"])
    private val newObjectButton: Button = Button(messages["new_object"])
    override val root = BorderPane()

    init {
        subscribe<NewObjectEvent> {
            Notifications.create()
                    .title(messages["saved"])
                    .text(messages["notification"])
                    .owner(root)
                    .showInformation()
            objectTable.items.setAll(it.objectList)
        }
        with(newObjectButton) {
            action {
                openNewObjectWindow()
            }
        }
        with(detailButton) {
            disableProperty().bind(
                    objectTable.selectionModel.selectedItemProperty().isNull
            )
            action {
                objectTable.selectedItem?.let {
                    openDetailWindow(objectTable.selectedItem!!)
                }
            }
        }

        with(objectTable) {
            placeholder = label(messages["empty_list"])
            column(messages["name"], ScanObject::name)
            column(messages["code"], ScanObject::code)
            column(messages["amount"], ScanObject::amount)
            column(messages["date"], ScanObject::created).cellFormat {
                text = it.toLocalDate().toString()
            }
            items = controller.objects()
            columnResizePolicy = SmartResize.POLICY
            onDoubleClick { openDetailWindow(objectTable.selectedItem!!) }
        }

        with(searchField) {
            useMaxWidth = true
        }

        with(root) {
            top {
                menubar {
                    menu(messages["menu"]) {
                        item(messages["scan"]).action { openInternalWindow(ScanFragment::class) }
                        item(messages["view_details"]).action {
                            objectTable.selectedItem?.let {
                                openDetailWindow(objectTable.selectedItem!!)
                            }
                        }
                        item(messages["export"], "Shortcut+S").action {
                            val location = chooseDirectory(messages["pickDir"])?.absolutePath
                            var columnNames = ""
                            objectTable.columns.forEach {
                                columnNames += it.text + ","
                            }
                            location?.let {
                                dataController.save(location, columnNames + "Events")
                            }
                        }
                        item(messages["import"], "Shortcut+O").action {
                            val files = chooseFile(messages["pickFile"], arrayOf(FileChooser.ExtensionFilter("Comma Separated Values", "*.csv")))
                            dataController.load(files)
                        }
                    }
                }
            }

            center {
                vbox {
                    hbox {
                        this += button(messages["search"], FontAwesomeIconView(FontAwesomeIcon.SEARCH)) {
                            disableProperty().bind(searchField.text.isNullOrBlank().toProperty())
                            action {
                                controller.search(searchField.text)
                                subscribe<ObjectSearchEvent> {
                                    objectTable.items = it.objects
                                }
                            }
                        }
                        this += searchField
                    }

                    this += objectTable
                }
            }

            bottom {
                hbox {
                    this += detailButton
                    this += newObjectButton
                    this += button(messages["refresh"], FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        action {
                            objectTable.items = controller.objects()
                        }
                    }
                }
            }
        }

    }

    private fun openDetailWindow(selectedObject: ScanObject) {
        ScanController.setObject(selectedObject)
        this.find<ObjectDetailFragment>().openModal()
    }

    private fun openNewObjectWindow() {
        this.find<ScanFragment>().openModal()
    }
}