package eventbus

import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 9/07/17
 */
object ObjectSearchRequest : FXEvent(EventBus.RunOn.BackgroundThread)