package eventbus

import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 5/07/17
 */
object NewObjectRequest : FXEvent(EventBus.RunOn.BackgroundThread)