package eventbus

import javafx.collections.ObservableList
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 5/07/17
 */
class ObjectListEvent(val objectList: ObservableList<ScanObject>): FXEvent()