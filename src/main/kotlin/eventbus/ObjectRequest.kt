package eventbus

import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 4/07/17
 */
object ObjectRequest : FXEvent(EventBus.RunOn.BackgroundThread)