package eventbus

import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 4/07/17
 */
class ObjectSendEvent(val requestedObject: ScanObject) : FXEvent()