package eventbus

import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 5/07/17
 */
object ObjectListRequest : FXEvent(EventBus.RunOn.BackgroundThread)