package eventbus

import javafx.collections.ObservableList
import models.ScanObject
import tornadofx.*

/**
 * @author Olivier Brabanders
 * Created on 9/07/17
 */
class ObjectSearchEvent(val objects: ObservableList<ScanObject>) : FXEvent()