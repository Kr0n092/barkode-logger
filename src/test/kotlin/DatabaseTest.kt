import io.kotlintest.TestCaseContext
import io.kotlintest.matchers.*
import io.kotlintest.specs.StringSpec
import models.DatabaseControl
import models.Event
import models.ScanObject
import models.ScanObjectFactory
import org.joda.time.DateTime

/**
 * @author Olivier Brabanders
 * Created on 8/08/17
 */
class DatabaseTest : StringSpec() {
    private val CODE = "123456789"
    private val NAME = "NAME"
    private val objectList = ScanObjectFactory.objectList()

    val insertInterceptor: (TestCaseContext, () -> Unit) -> Unit = { _, testCase ->
        // before
        DatabaseControl.wipe()
        DatabaseControl.add(objectList)
        testCase()
        // after
        DatabaseControl.wipe()
    }

    init {
        "adding object should reflect in database" {
            DatabaseControl.objects().size shouldBe objectList.size
            DatabaseControl.add(ScanObject(CODE, NAME))
            DatabaseControl.objects().size shouldBe objectList.size+1
        }.config(interceptors = listOf(insertInterceptor))

        "wiping the database should remove all objects" {
            DatabaseControl.wipe()
            DatabaseControl.objects() should beEmpty<ScanObject>()
        }.config(interceptors = listOf(insertInterceptor))

        "removing an object should reflect in database" {
            DatabaseControl.objects().size shouldBe objectList.size
            val toDelete = ScanObject(CODE.reversed(), NAME.reversed())
            DatabaseControl.add(toDelete)
            DatabaseControl.objects().size shouldBe (objectList + toDelete).size
            DatabaseControl.delete(toDelete)
            DatabaseControl.objects().size shouldBe objectList.size
        }.config(interceptors = listOf(insertInterceptor))

        "searching for an object should yield the desired result" {
            val toSearch = ScanObject(CODE.reversed(), NAME.reversed())
            DatabaseControl.add(toSearch)
            val result = DatabaseControl.select("EM")
            result.size shouldBe 1
            result[0] should beEqual(toSearch)
        }.config(interceptors = listOf(insertInterceptor))

        "changing an object should be reflected in the database" {
            val toChange = ScanObject(CODE.reversed(), NAME.reversed())
            DatabaseControl.add(toChange)
            DatabaseControl.updateObject(toChange, CODE.reversed(), NAME.reversed(), 2)
            val result: ScanObject = DatabaseControl.select(NAME.reversed())[0]
            result.amount shouldEqual 2
        }

        "adding an event to an object should be reflected in the database" {
            val testObject = ScanObject(CODE, NAME)
            DatabaseControl.add(testObject)
            val testEvent = Event(DateTime(2010,5,15,0,0),"test event")
            testObject.addEvent(testEvent.date,testEvent.text)
            DatabaseControl.addEvent(testObject)
            val result: ScanObject = DatabaseControl.select(testObject.name)[0]
            result.events[0].text shouldEqual testEvent.text
        }
    }

    fun beEqual(obj: ScanObject) = object: Matcher<ScanObject> {
        override fun test(value: ScanObject): Result {
            return Result(
                    value.name == obj.name &&
                            value.code == obj.code &&
                            value.amount == obj.amount &&
                            value.created.toLocalDate() == obj.created.toLocalDate() &&
                            value.events.size == obj.events.size,
                    "ScanObject $value should be equal to $obj"
            )
        }
    }
}