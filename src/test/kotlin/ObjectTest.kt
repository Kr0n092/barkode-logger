import io.kotlintest.TestCaseContext
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec
import models.Event
import models.ScanObject
import org.joda.time.DateTime

/**
 * @author Olivier Brabanders
 * Created on 8/08/17
 */
class ObjectTest : StringSpec() {
    private lateinit var testObject: ScanObject

    val emptyInterceptor: (TestCaseContext, () -> Unit) -> Unit = { _, testCase ->
        // before
        this.testObject = ScanObject()
        testCase()
        // after
    }

    val newObjectInterceptor: (TestCaseContext, () -> Unit) -> Unit = { _, testCase ->
        // before
        this.testObject = ScanObject("123456789", "test", DateTime(2010, 5, 15, 0, 0), 13)
        testCase()
        // after
    }

    init {
        "empty object should have default parameters" {
            testObject.name shouldBe ""
            testObject.code shouldBe ""
            testObject.created.toLocalDate() shouldBe DateTime.now().toLocalDate()
            testObject.amount shouldBe 1
            testObject.events shouldBe arrayListOf<Event>()
        }.config(interceptors = listOf(emptyInterceptor))

        "object with parameters should have them set" {
            testObject.name shouldBe "test"
            testObject.code shouldBe "123456789"
            testObject.created shouldBe DateTime(2010, 5, 15, 0, 0)
            testObject.amount shouldBe 13
            testObject.events shouldBe arrayListOf<Event>()
        }.config(interceptors = listOf(newObjectInterceptor))

        "adding event should create it and add it to the object's history" {
            val testDate: DateTime = DateTime(2010, 5, 15, 0, 0)
            testObject.addEvent(testDate, "test event")
            testObject.events shouldBe arrayListOf(Event(testDate, "test event"))
        }.config(interceptors = listOf(emptyInterceptor))
    }
}