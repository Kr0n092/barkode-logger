import io.kotlintest.ProjectConfig
import models.DatabaseControl

/**
 * @author Olivier Brabanders
 * Created on 8/08/17
 */
object TestConfig: ProjectConfig() {
    override fun beforeAll() {
        DatabaseControl.setUp()
    }
}